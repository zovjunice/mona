'use strict';

/**
 * @ngdoc overview
 * @name dragdropApp
 * @description
 * # dragdropApp
 *
 * Main module of the application.
 */
angular
    .module('dragdropApp', [
        'ngAnimate',
        'ngCookies',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngTouch',
        'ngDragDrop',
        'ui.bootstrap',
        'gridster'
    ])
    .config(function ($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'views/main.html',
                controller: 'MainCtrl',
                resolve:{
                  panelHtml: function(html){
                   return  html.panelHtml();
                  },
                  tabHtml: function(html){
                    return  html.tabHtml();
                  }
                }
            })
            .when('/about', {
                templateUrl: 'views/about.html',
                controller: 'AboutCtrl'
            })
            .otherwise({
                redirectTo: '/'
            });
    });

