'use strict';

/**
 * @ngdoc function
 * @name dragdropApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the dragdropApp
 */
angular.module('dragdropApp')
  .controller('MainCtrl', function ($scope, $timeout, panelHtml, tabHtml) {
    $scope.images = [
      {'thumb': '1.png'},
      {'thumb': '2.png'},
      {'thumb': '3.png'},
      {'thumb': '4.png'}
    ]
    $scope.list1 = [];
    $scope.list4 = [];
    angular.forEach($scope.images, function (val, key) {
      $scope.list1.push({});
    });







    $scope.list2 = [
      { 'title': 'Item 1', 'drag': true, html: "", class: "btn btn-info btn-draggable" },
      { 'title': 'Item 2', 'drag': true, html: "", class: "btn btn-info btn-draggable" },
      { 'title': 'Item 3', 'drag': true, html: "", class: "btn btn-info btn-draggable" },
      { 'title': 'Item 4', 'drag': true, html: "", class: "btn btn-info btn-draggable" }
    ];
    $scope.list3 = [
      { 'title': 'Item 1', 'drag': true, html: "", class: "alert alert-success" },
      { 'title': 'Item 2', 'drag': true, html: "", class: "alert alert-success" },
      { 'title': 'Panel', 'drag': true, html: panelHtml, class: 'alert alert-success'},
      { 'title': 'Tab', 'drag': true, html: tabHtml, class: 'alert alert-success'}
    ];
    debugger
    $scope.tabs = [
      {title: "prvi"},
      {title: "drugi"}
    ];
    $scope.panel = {
      title: "Naslov",
      content: "Content"

    };
    $scope.startCallback = function (event, ui, title) {
      console.log('You started draggin: ' + title.title);
      $scope.draggedTitle = title.title;
    };

    $scope.stopCallback = function (event, ui) {
      console.log('Why did you stop draggin me?');
    };

    $scope.dragCallback = function (event, ui) {
      console.log('hey, look I`m flying');
    };

    $scope.dropCallback = function (event, ui) {
      console.log('hey, you dumped me :-(', $scope.draggedTitle);
    };

    $scope.overCallback = function (event, ui) {
      console.log('Look, I`m over you');
    };

    $scope.outCallback = function (event, ui) {
      console.log('I`m not, hehe');
    };

    $scope.gridsterOptions = {
      margins: [20, 20],
      columns: 4,
      draggable: {
        handle: 'h3'
      }
    };

    $scope.dashboards = {
      '1': {
        id: '1',
        name: 'Home',
        widgets: [
          {
            col: 0,
            row: 0,
            sizeY: 1,
            sizeX: 1,
            name: "Widget 1"
          },
          {
            col: 2,
            row: 1,
            sizeY: 1,
            sizeX: 1,
            name: "Widget 2"
          }
        ]
      },
      '2': {
        id: '2',
        name: 'Other',
        widgets: [
          {
            col: 1,
            row: 1,
            sizeY: 1,
            sizeX: 2,
            name: "Other Widget 1"
          },
          {
            col: 1,
            row: 3,
            sizeY: 1,
            sizeX: 1,
            name: "Other Widget 2"
          }
        ]
      }
    };

    $scope.clear = function () {
      $scope.dashboard.widgets = [];
    };

    $scope.addWidget = function () {
      $scope.dashboard.widgets.push({
        name: "New Widget",
        sizeX: 1,
        sizeY: 1
      });
    };

    $scope.$watch('selectedDashboardId', function (newVal, oldVal) {
      if (newVal !== oldVal) {
        $scope.dashboard = $scope.dashboards[newVal];
      } else {
        $scope.dashboard = $scope.dashboards[1];
      }
    });

    // init dashboard
    $scope.selectedDashboardId = '1';

  })

  .controller('CustomWidgetCtrl', ['$scope', '$modal',
    function ($scope, $modal) {

      $scope.remove = function (widget) {
        $scope.dashboard.widgets.splice($scope.dashboard.widgets.indexOf(widget), 1);
      };

      $scope.openSettings = function (widget) {
        $modal.open({
          scope: $scope,
          templateUrl: 'views/widget_settings.html',
          controller: 'WidgetSettingsCtrl',
          resolve: {
            widget: function () {
              return widget;
            }
          }
        });
      };

    }
  ])

  .controller('WidgetSettingsCtrl', ['$scope', '$timeout', '$rootScope', '$modalInstance', 'widget',
    function ($scope, $timeout, $rootScope, $modalInstance, widget) {
      $scope.widget = widget;

      $scope.form = {
        name: widget.name,
        sizeX: widget.sizeX,
        sizeY: widget.sizeY,
        col: widget.col,
        row: widget.row
      };

      $scope.sizeOptions = [
        {
          id: '1',
          name: '1'
        },
        {
          id: '2',
          name: '2'
        },
        {
          id: '3',
          name: '3'
        },
        {
          id: '4',
          name: '4'
        }
      ];

      $scope.dismiss = function () {
        $modalInstance.dismiss();
      };

      $scope.remove = function () {
        $scope.dashboard.widgets.splice($scope.dashboard.widgets.indexOf(widget), 1);
        $modalInstance.close();
      };

      $scope.submit = function () {
        angular.extend(widget, $scope.form);

        $modalInstance.close(widget);
      };

    }
  ])

