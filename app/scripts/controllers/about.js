'use strict';

/**
 * @ngdoc function
 * @name dragdropApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the dragdropApp
 */
angular.module('dragdropApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
